#pragma once
#include<iostream>
#include<string>

using namespace std;	
class Pokemon
{
public:
	Pokemon();
	Pokemon(string name, int baseHp, int level, int baseDamage);
	string names;
	int baseHp;
	int hp;
	int maxHp;
	int level;
	int baseDamage;
	int exp;
	int expToNextLevel;

	void applyDamage(Pokemon* opponent);
	void gainExp();
	void levelUp();
};

