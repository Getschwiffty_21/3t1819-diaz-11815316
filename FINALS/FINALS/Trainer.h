#pragma once
#include<iostream>
#include<string>
#include <vector>
#include <conio.h>

using namespace std;
class Pokemon;

class Trainer
{
public:
	Trainer();
	Trainer(string name);
	string name;
	vector<Pokemon*> myPokemons;
	int x, y;
	string location;
	bool isInTown = true;
	
	
	void displayPokemon();
	void goPokemonCenter();
	void move();
	bool capturePokemon(Pokemon* wildPokemon);
	bool encounterPokemon();
	bool run();
	bool changePokemon();

};

