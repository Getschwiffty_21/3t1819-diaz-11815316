#include "Trainer.h"
#include "Pokemon.h"

Trainer::Trainer()
{
	this->name = "";
}

Trainer::Trainer(string name)
{
	this->name = name;
	this->location = "LittleRoot Town";
}

void Trainer::displayPokemon()
{
	for (int i = 0; i < this->myPokemons.size(); i++) 
	{
		cout << "Pokemon: " << this->myPokemons[i]->names << endl;
		cout << "Level: " << this->myPokemons[i]->level << endl;
		cout << "Exp: " << this->myPokemons[i]->exp << "/" << this->myPokemons[0]->expToNextLevel << endl;
		cout << "HP: " << this->myPokemons[i]->hp <<"/" << this->myPokemons[i]->maxHp<< endl;
		cout << "Attack: " << this->myPokemons[i]->baseDamage << endl << endl;
	}
}

void Trainer::goPokemonCenter() {
	cout << "Welcome to the pokemon center, we will heal up your pokemon to its full health...\n" << endl;
	for (int i = 0; i < this->myPokemons.size(); i++) 
	{
		this->myPokemons[i]->hp = this->myPokemons[i]->maxHp;
	}
	_getch();
	cout << "your pokemon is back to full health..." << endl;
}

void Trainer::move()
{
	int move;

		cout << "(" << this->x << ", " << this->y << ")" << endl;
		cout << this->location << endl << endl;
		cout << "1 = North" << endl;
		cout << "2 = South" << endl; 
		cout << "3 = East" << endl;
		cout << "4 = West" << endl << endl;
		cout << "Input (press 0 to go back): ";
		cin >> move;

		if (move == 1)
		{
			this->y = this->y + 1;
		}
		else if (move == 2)
		{
			this->y = this->y - 1;
		}
		else if (move == 3)
		{
			this->x = this->x + 1;
		}
		else if (move == 4)
		{
			this->x = this->x - 1;
		}

		if ((this->x > 2 || this->x < -2) || (this->y > 2 || this->y < -2)) 
		{
			this->location = "Wild Area";
			this->isInTown = false;
		}
		else 
		{
			this->location = "LittleRoot Town";
			this->isInTown = true;
		}

		system("cls");

}

bool Trainer::encounterPokemon() {
	if (this->isInTown == false)
	{
		if (rand() % 100 + 1 <= 50)
		{
			return true;
		}
	}
	else false;
}

bool Trainer::capturePokemon(Pokemon* wildPokemon)
{
	cout << "You threw a pokeball" << endl;
	if (rand() % 100 + 1 <= 30)
	{
		cout << "you successfully captured " << wildPokemon->names << endl;
		this->myPokemons.push_back(wildPokemon);
		return true;
	}
	else
	{
		cout << "but " << wildPokemon->names << " got out" << endl;
		return false;
	}
}

bool Trainer::run() {
	cout << "You ran away" << endl;
	return false;
}

bool Trainer::changePokemon()
{
	int size = 0;
	for (int i = 0; i < this->myPokemons.size(); i++)
	{
		if (this->myPokemons[i]->hp <= 0)
		{
			size = size + 1;
		}
	}
	
	if (size == this->myPokemons.size()) 
	{
		cout << this->name << " blacked out" << endl;
		this->goPokemonCenter();
		this->x = 0;
		this->y = 0;
		this->location = "LittleRoot Town";
		this->isInTown = true;
		return true;
	}
	else
	{
		for (int i = 0; i < this->myPokemons.size() - 1; i++)
		{
			swap(this->myPokemons[i], this->myPokemons[i + 1]);
		}
		return false;
	}
}




//void Trainer::displayTrainerName()
//{
//	cout << "\nTrainer: " << this->name;
//}
//

