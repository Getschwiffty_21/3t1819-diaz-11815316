#include<iostream>
#include<string>
#include<conio.h>
#include <vector>
#include<time.h>

#include"Trainer.h"
#include"Pokemon.h"


using namespace std;

void pokemonBattle(Trainer* myTrainer) 
{
	int randomPokemon = rand() % 12;
	int choiceBattle;
	int randomLvl = rand() % 10 + 1;
	bool isNotRun = true;
	bool isAllFaint = false;
	Pokemon* wildPokemon[12];
	wildPokemon[0] = new Pokemon("Entei", 115 + randomLvl, randomLvl, 115 + randomLvl);
	wildPokemon[1] = new Pokemon("Raikou", 90 + randomLvl, randomLvl, 85 + randomLvl);
	wildPokemon[2] = new Pokemon("Suicune", 100 + randomLvl, randomLvl, 75 + randomLvl);
	wildPokemon[3] = new Pokemon("Deoxys", 80 + randomLvl, randomLvl, 150 + randomLvl);
	wildPokemon[4] = new Pokemon("Lugia", 106 + randomLvl, randomLvl, 90 + randomLvl);
	wildPokemon[5] = new Pokemon("Ho-oh", 106 + randomLvl, randomLvl, 130 + randomLvl);
	wildPokemon[6] = new Pokemon("Kyogore", 100 + randomLvl, randomLvl, 100 + randomLvl);
	wildPokemon[7] = new Pokemon("Groudon", 100 + randomLvl, randomLvl, 150 + randomLvl);
	wildPokemon[8] = new Pokemon("Rayquaza", 108 + randomLvl, randomLvl, 150 + randomLvl);
	wildPokemon[9] = new Pokemon("Zygard", 108 + randomLvl, randomLvl, 100 + randomLvl);
	wildPokemon[10] = new Pokemon("Reshiram", 100 + randomLvl, randomLvl, 120 + randomLvl);
	wildPokemon[11] = new Pokemon("Zekrom", 100 + randomLvl, randomLvl, 150 + randomLvl);
	bool isCaptured = false;

	cout << "A wild " << wildPokemon[randomPokemon]->names << " appeared" << endl;
	while (wildPokemon[randomPokemon]->hp > 0 && isCaptured == false && isNotRun == true && isAllFaint == false)
	{ 
		cout << "What would you like to do?" << endl;
		cout << "1 = Battle" << endl;
		cout << "2 = Capture" << endl;
		cout << "3 = Run" << endl << endl;
		cout << "Input: " << endl;
		cin >> choiceBattle;

		if (choiceBattle == 1)
		{
			myTrainer->myPokemons[0]->applyDamage(wildPokemon[randomPokemon]);
		}
		else if (choiceBattle == 2)
		{
			isCaptured = myTrainer->capturePokemon(wildPokemon[randomPokemon]);
		}
		else if (choiceBattle == 3)
		{
			isNotRun = myTrainer->run();
		}

		if (wildPokemon[randomPokemon]->hp > 0 && isCaptured == false && isNotRun == true)
		{
			wildPokemon[randomPokemon]->applyDamage(myTrainer->myPokemons[0]);
		}
		
		if (myTrainer->myPokemons[0]->hp <= 0) 
		{
			cout << myTrainer->myPokemons[0]->names << " fainted " << endl;
			isAllFaint = myTrainer->changePokemon();


		}

	}
	
	if (isCaptured == false && isNotRun == true && isAllFaint == false)
	{
		cout << "Wild " << wildPokemon[randomPokemon]->names << " fainted" << endl;
		myTrainer->myPokemons[0]->gainExp();
	}

	_getch();
}
void mainAction(Trainer* myTrainer)
{
	int choiceMainAction;
	cout << "What do you want to do brave adventurer?" << endl;
	cout << "Current Location: " << myTrainer->location << endl;
	cout << "1 = Display Pokemon Stats" << endl;
	cout << "2 = Move" << endl;
	
	if (myTrainer->isInTown == true)
	{
		cout << "3 = Pokemon Center" << endl;
	}
	cout << "CHOICE: ";
	cin >> choiceMainAction;

	if (choiceMainAction == 1)
	{
		myTrainer->displayPokemon();
		_getch();
	}
	else if (choiceMainAction == 3 && myTrainer->isInTown == true)
	{
		myTrainer->goPokemonCenter();
	}
	else if (choiceMainAction == 2)
	{
		myTrainer->move();
		if (myTrainer->encounterPokemon() == true)
		{
			pokemonBattle(myTrainer);
		}
	}

	system("CLS");
}

int main()
{
	srand(time(NULL));
	string inputName;
	int choiceStarterPokemon;

	cout << "Welcome to town Little Root!" << endl;
	cout << "What's your name brave adventurer? ";
	cin >> inputName;
	Trainer* myTrainer = new Trainer(inputName);
	_getch();
	system("CLS");

	cout << "I have three starter pokemons right now." << endl;
	cout << "Which one will you choose?" << endl;
	cout << "1 = Torchic" << endl;
	cout << "2 = Treecko" << endl;
	cout << "3 = Mudkip" << endl;
	cout << "Who will you choose? ";
	cin >> choiceStarterPokemon;
	_getch();
	system("CLS");

	Pokemon* myPokemon[3] = { new Pokemon("Torchic", 45, 1, 60), new Pokemon("Treecko", 40, 1, 45), new Pokemon("Mudkip", 50, 1, 70) };
	myTrainer->myPokemons.push_back(myPokemon[choiceStarterPokemon -1]);
	cout << "Congratulations brave adventurer, " << myTrainer->name << "." << endl;
	cout << "You have chosen " << myTrainer->myPokemons[0]->names << " as your starter pokemon!";
	_getch();
	system("CLS");

	while (true) {
		mainAction(myTrainer);
	}

	_getch();
 }