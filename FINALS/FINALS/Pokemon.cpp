#include "Pokemon.h"

Pokemon::Pokemon()
{
}


Pokemon::Pokemon(string name, int baseHp, int level, int baseDamage)
{
	this->names = name;
	this->baseHp = baseHp;
	this->hp = baseHp;
	this->maxHp = this->hp;
	this->level = level;
	this->baseDamage = baseDamage;
	this->exp = 0;
	this->expToNextLevel = 20;
}

void Pokemon::applyDamage(Pokemon * opponent) 
{
	if (rand() % 100 + 1 <= 80)
	{
		cout << this->names << " attacked " << opponent->names << endl;
		cout << this->names << " dealt " << this->baseDamage << endl;
		opponent->hp -= this->baseDamage;
		if (opponent->hp < 0)
		{
			opponent->hp = 0;
		}
	}
	else
	{
		cout << this->names << " missed." << endl;
	}
}

void Pokemon::levelUp() {

	if (this->exp >= this->expToNextLevel) 
	{
		cout << this->names << " leveled up to " << this->level + 1 << endl;
		this->maxHp = this->maxHp + this->baseHp * 0.15f;
		this->baseDamage = this->baseDamage + this->baseDamage * 0.1f;
		this->expToNextLevel = this->expToNextLevel + this->expToNextLevel * 0.2f;
		this->exp = 0;
		this->level = this->level + 1;
	}
}

void Pokemon::gainExp()
{
	cout << this->names << " gained " << this->expToNextLevel * 0.20f << " exp";
	this->exp = this->exp + this->expToNextLevel * 0.20f;
	this->levelUp();
}